﻿//This is the class that allows entity to create a context for the database.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace BibleVerseApp.Models
{
    public class BibleDbContext : DbContext
    {
        public DbSet<BibleVerse> dbVerse { get; set; }
    }
}