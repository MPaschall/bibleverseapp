﻿//This model class allows the creation of object arguments.
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BibleVerseApp.Models
{
    public class BibleVerse
    {
        [Key]
        public string Testament { get; set; }
        public string Book { get; set; }
        public int Chapter { get; set; }
        public int VerseNum { get; set; }
        public string VerseText { get; set; }
    }
}