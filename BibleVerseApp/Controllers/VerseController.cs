﻿/*This controller gets arguments from the models class, performs actions, 
and sends them to the view that is returned.*/
using BibleVerseApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BibleVerseApp.Controllers
{
    public class BibleController : Controller
    {
        public ActionResult VerseEntry()
        {
            return View();
        }

        public ActionResult VerseList()
        {
            using (BibleDbContext db = new BibleDbContext())
            {
                return View(db.dbVerse.ToList());
            }

        }

        [HttpPost]
        public ActionResult VerseEntry(BibleVerse verse)
        {
            if (ModelState.IsValid)
            {
                using (BibleDbContext db = new BibleDbContext())
                {
                    db.dbVerse.Add(verse);
                    db.SaveChanges();
                }

                ModelState.Clear();
                ViewBag.Message = verse.Book + " " + verse.Chapter + " " + verse.VerseNum + " " + "Successfully Entered.";
            }
            return View();
        }

        public ActionResult SearchVerse(/*string book, int chapter, int vnum*/)
        {
            
                return View(/*db.dbVerse.Where(v => v.Book == book && v.Chapter == chapter && v.VerseNum == vnum)*/);
           
        }
    }
}
